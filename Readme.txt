﻿Sébastien Rivault
Yann Christeaut 


Les fonctionnalité présentes :
* afficher la liste des tâches
* voir le statut d’avancement des tâches
* créer/modifier des tâches et de gérer leurs niveaux de priorité
* gérer les deadlines des tâches 
* travailler à plusieurs sur une tâche et assigner des sous tâches aux participants
* partager la tâche avec d’autres applications/utilisateurs 
* utiliser un service centralisé et une BDD local pour l’utilisation hors ligne
* (exposer les données de l’application par un content provider)


Les fonctionnalité non présentes :


* un système de rappel / d’alerte
* afficher un widget sur l’écran d'accueil affichant une sélection de tâches