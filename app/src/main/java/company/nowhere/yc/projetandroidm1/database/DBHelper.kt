package company.nowhere.yc.projetandroidm1.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import company.nowhere.yc.projetandroidm1.models.*
import company.nowhere.yc.projetandroidm1.tools.Singleton

/*
    Encapsule les acces à la BD
    @param context : Context
    @extend SQliteOpenHelper
 */
class DBHelper private constructor(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(ArbreDBHelper.SQL_CREATE_ENTRIES)
        db.execSQL(NoeudDBHelper.SQL_CREATE_ENTRIES)
        db.execSQL(UsersDBHelper.SQL_CREATE_ENTRIES_USER)
        db.execSQL(UsersDBHelper.SQL_CREATE_ENTRIES_DEVICE)
        db.execSQL(ArbreViewerDBHelper.SQL_CREATE_ENTRIES)

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(ArbreDBHelper.SQL_DELETE_ENTRIES)
        db.execSQL(NoeudDBHelper.SQL_DELETE_ENTRIES)
        db.execSQL(UsersDBHelper.SQL_DELETE_ENTRIES_USER)
        db.execSQL(UsersDBHelper.SQL_DELETE_ENTRIES_DEVICE)
        db.execSQL(ArbreViewerDBHelper.SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object : Singleton<DBHelper, Context>(::DBHelper) {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 55
        const val DATABASE_NAME = "database.db"
    }
}