package company.nowhere.yc.projetandroidm1.database

import android.provider.BaseColumns

object DBContract {

    object ArbreEntry : BaseColumns {
        const val TABLE_NAME = "arbre"
        const val COLUMN_ID_NOEUD = "idNoeud"
        const val COLUMN_ID_PROPRIETAIRE = "idProprietaire"
        const val COLUMN_IS_SUB_TREE = "isSubTree"
    }

    object UserEntry : BaseColumns {
        const val TABLE_NAME = "user"
        const val COLUMN_NOM = "nom"
        const val COLUMN_PRENOM = "prenom"
        const val COLUMN_MAIL = "mail"
        const val COLUMN_PHONE = "phone"
        const val COLUMN_USERNAME = "username"
        const val COLUMN_PASSWD = "password"
    }

    object ArbreViewerEntry : BaseColumns {
        const val TABLE_NAME = "viewers"
        const val COLUMN_ID_ARBRE = "idTree"
        const val COLUMN_ID_USER = "idUser"
        const val COLUMN_CAN_MODIFY = "canModify"
    }

    object NoeudEntry : BaseColumns {
        const val TABLE_NAME = "noeud"
        const val COLUMN_TITRE = "titre"
        const val COLUMN_DESC = "description"
        const val COLUMN_LIEU = "lieu"
        const val COLUMN_CREATION = "creation"
        const val COLUMN_DEADLINE = "deadline"
        const val COLUMN_PRIORITY = "priorite"
        const val COLUMN_AVANCEMENT = "avancement"
        const val COLUMN_IS_SUB_TREE = "isSubTree"
        const val COLUMN_ID_PERE = "idPere"
        const val COLUMN_ID_ASSIGNEE = "assigneeId"
        // used only for route data to specific user
        const val COLUMN_ID_TREE = "idTree"
    }

    object DeviceEntry : BaseColumns {
        const val TABLE_NAME = "device_user"
        const val COLUMN_ID_USER = "idUser"
        const val COLUMN_ID_DEVICE = "externalId"
    }
}