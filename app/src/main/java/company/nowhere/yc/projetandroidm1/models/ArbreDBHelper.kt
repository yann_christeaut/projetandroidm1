package company.nowhere.yc.projetandroidm1.models

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import android.provider.BaseColumns
import company.nowhere.yc.projetandroidm1.database.DBContract
import company.nowhere.yc.projetandroidm1.database.DBHelper
import company.nowhere.yc.projetandroidm1.tools.Singleton

/*
    Gere toutes les interactions avec la table Arbre dans la BD
    @param DBHelper : instance de la classe DBHelper
 */
class ArbreDBHelper private constructor(context: Context) {

    private val context = context
    private val helper = DBHelper.getInstance(context)
    private val userDB = UsersDBHelper.getInstance(context)
    private val viewerDB = ArbreViewerDBHelper.getInstance(context)

    /*
    Insert un arbre dans la BD
    @param arbre : Arbre a inserer
    @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun insertTree(arbre: Arbre) {
        val nodeDB = NoeudDBHelper.getInstance(context)
        // Gets the data repository in write mode
        // Insert the new row, returning the primary key value of the new row
        nodeDB.insertNoeud(arbre.racine, arbre)
        if (arbre.participants.size > 0)
            viewerDB.insertViewers(arbre.participants, true, arbre.id)
        if (arbre.viewers.size > 0)
            viewerDB.insertViewers(arbre.viewers, false, arbre.id)

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(DBContract.ArbreEntry.COLUMN_ID_NOEUD, arbre.racine.id)
        values.put(DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE, arbre.proprietaire.id)
        values.put(DBContract.ArbreEntry.COLUMN_IS_SUB_TREE,arbre.isSubTree)

        val db = helper.writableDatabase
        arbre.id = db.insert(DBContract.ArbreEntry.TABLE_NAME, null, values)
        db.close()
    }

    /*
    Lit un arbre dans la BD par son id
    @param arbreId : Int id de l'arbre a lire dans la BD
    @return Arbre? une instance d'arbre s'il existe dans la BD
     */
    fun readTree(arbreId : Long, readRoot : Boolean): Arbre? {
        val nodeDB = NoeudDBHelper.getInstance(context)
        var arbre : Arbre? = null
        val db = helper.readableDatabase
        val cursorArbre: Cursor?
        try {
            cursorArbre = db.rawQuery("SELECT * FROM " + DBContract.ArbreEntry.TABLE_NAME +
                    " WHERE " + BaseColumns._ID + "='" + arbreId + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.close()
            return arbre
        }

        val racineId : Long
        val proprioId : Long
        if (cursorArbre!!.moveToFirst() && !cursorArbre.isAfterLast) {
            racineId = cursorArbre.getLong(cursorArbre.getColumnIndex(DBContract.ArbreEntry.COLUMN_ID_NOEUD))
            proprioId = cursorArbre.getLong(cursorArbre.getColumnIndex(DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE))
            val isSubTree = cursorArbre.getInt(cursorArbre.getColumnIndex(DBContract.ArbreEntry.COLUMN_IS_SUB_TREE)) != 0
            cursorArbre.close()
            db.close()
            val proprio = userDB.readUser(proprioId)
            val participants = viewerDB.readViewers(arbreId, true)
            val viewers = viewerDB.readViewers(arbreId, false)
            arbre = Arbre(arbreId, null, proprio, participants, viewers, isSubTree)

            if(readRoot)
                arbre.racine = nodeDB.readNoeud(racineId, arbre, false, false)
        }

        return arbre
    }

    /*
    Lit un arbre dans la BD par son id
    @param arbreId : Int id de l'arbre a lire dans la BD
    @return Arbre? une instance d'arbre s'il existe dans la BD
     */
    fun readTrees(userId : Long): ArrayList<Arbre> {
        val nodeDB = NoeudDBHelper.getInstance(context)
        val trees = ArrayList<Arbre>()
        val db = helper.readableDatabase
        val whereArgs = arrayOf(userId.toString())
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("SELECT * " +
                    "FROM " + DBContract.ArbreEntry.TABLE_NAME + " a , "
                    + DBContract.ArbreViewerEntry.TABLE_NAME + " v "+
                    " WHERE a." + BaseColumns._ID + "=v." + DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE +
                    " AND v." + DBContract.ArbreViewerEntry.COLUMN_ID_USER + "= ?"
                    , whereArgs)
        } catch (e: SQLiteException) {
            db.close()
            return trees
        }

        if (cursor!!.moveToFirst()){
            var tree : Arbre
            while (!cursor.isAfterLast) {
                val arbreId = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID))
                val racineId = cursor.getLong(cursor.getColumnIndex(DBContract.ArbreEntry.COLUMN_ID_NOEUD))
                val proprioId = cursor.getLong(cursor.getColumnIndex(DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE))
                val proprio = userDB.readUser(proprioId)
                val participants = viewerDB.readViewers(arbreId, true)
                val viewers = viewerDB.readViewers(arbreId, false)
                val isSubTree = cursor.getInt(cursor.getColumnIndex(DBContract.ArbreEntry.COLUMN_IS_SUB_TREE)) != 0
                tree = Arbre(arbreId, null, proprio, participants, viewers, isSubTree)
                tree.racine = nodeDB.readNoeud(racineId, tree, false, false)
                trees.add(tree)
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()
        trees.addAll(readTreesByProprio(userId))
        return trees
    }

    private fun readTreesByProprio(userId: Long) : ArrayList<Arbre> {
        val nodeDB = NoeudDBHelper.getInstance(context)
        val trees = ArrayList<Arbre>()
        val db = helper.readableDatabase
        val whereArgs = arrayOf(userId.toString())
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("SELECT * " +
                    "FROM " + DBContract.ArbreEntry.TABLE_NAME +
                    " WHERE " + DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE +
                    "= ?"
                    , whereArgs)
        } catch (e: SQLiteException) {
            db.close()
            return trees
        }

        if (cursor!!.moveToFirst()) {
            var tree : Arbre
            while (!cursor.isAfterLast) {
                val arbreId = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID))
                val racineId = cursor.getLong(cursor.getColumnIndex(DBContract.ArbreEntry.COLUMN_ID_NOEUD))
                val proprio = userDB.readUser(userId)
                val participants = viewerDB.readViewers(arbreId, true)
                val viewers = viewerDB.readViewers(arbreId, false)
                val isSubTree = cursor.getInt(cursor.getColumnIndex(DBContract.ArbreEntry.COLUMN_IS_SUB_TREE)) != 0
                tree = Arbre(arbreId, null, proprio, participants, viewers, isSubTree)
                tree.racine = nodeDB.readNoeud(racineId, tree, false, false)
                trees.add(tree)
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()
        return trees
    }

    /*
    Lit un arbre dans la BD par l'id de sa racine
    @param rootId : Int id de sa racine lire dans la BD
    @return Arbre? une instance d'arbre s'il existe dans la BD
     */
    fun readTreeByRoot(rootId: Long, tree : Arbre): Arbre? {
        var arbre : Arbre? = null
        val db = helper.readableDatabase
        val cursorArbre: Cursor?
        try {
            cursorArbre = db.rawQuery("SELECT * FROM " + DBContract.ArbreEntry.TABLE_NAME +
                    " WHERE " + DBContract.ArbreEntry.COLUMN_ID_NOEUD + "='" + rootId + "'", null)
        } catch (e: SQLiteException) {
            db.close()
            return null
        }

        val arbreId : Long
        val proprioId : Long
        val isSubTree : Boolean
        if (cursorArbre!!.moveToFirst() && !cursorArbre.isAfterLast) {
            arbreId = cursorArbre.getLong(cursorArbre.getColumnIndex(BaseColumns._ID))
            proprioId = cursorArbre.getLong(cursorArbre.getColumnIndex(DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE))
            val proprio = userDB.readUser(proprioId)
            val participants = viewerDB.readViewers(arbreId, true)
            participants.addAll(tree.participants)
            val viewers = viewerDB.readViewers(arbreId, false)
            viewers.addAll(tree.viewers)
            isSubTree = cursorArbre.getInt(cursorArbre.getColumnIndex(DBContract.ArbreEntry.COLUMN_IS_SUB_TREE)) != 0
            arbre = Arbre(arbreId, null, proprio, participants, viewers, isSubTree)
        }
        cursorArbre.close()
        db.close()
        return arbre
    }

    /*
    Supprime un arbre dans la BD
    @param arbre : Arbre instance a supprimer
    @return Boolean true si l'arbre a été supprimé
    @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun deleteTree(arbre: Arbre, deleteRoot : Boolean) {
        val nodeDB = NoeudDBHelper.getInstance(context)
        // Gets the data repository in write mode
        val db = helper.writableDatabase
        // Define 'where' part of query.
        val whereClause = BaseColumns._ID + " = ?"
        // Specify arguments in placeholder order.
        val whereArgs = arrayOf(arbre.id.toString())
        // Issue SQL statement.
        db.delete(DBContract.ArbreEntry.TABLE_NAME, whereClause, whereArgs)
        db.close()
        viewerDB.deleteAllViewers(arbre)
        if (deleteRoot)
            nodeDB.deleteNoeud(arbre.racine)
    }

    companion object : Singleton<ArbreDBHelper, Context>(::ArbreDBHelper) {

        const val SQL_CREATE_ENTRIES =
                "CREATE TABLE ${DBContract.ArbreEntry.TABLE_NAME} (" +
                        "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                        "${DBContract.ArbreEntry.COLUMN_ID_NOEUD} INTEGER NOT NULL," +
                        "${DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE} INTEGER NOT NULL," +
                        "${DBContract.ArbreEntry.COLUMN_IS_SUB_TREE} INTEGER NOT NULL," +
                        "FOREIGN KEY(${DBContract.ArbreEntry.COLUMN_ID_PROPRIETAIRE}) " +
                        "REFERENCES ${DBContract.UserEntry.TABLE_NAME}(${BaseColumns._ID}) ON DELETE CASCADE, " +
                        "FOREIGN KEY(${DBContract.ArbreEntry.COLUMN_ID_NOEUD}) " +
                        "REFERENCES ${DBContract.NoeudEntry.TABLE_NAME}(${BaseColumns._ID}) ON DELETE CASCADE)"

        const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DBContract.ArbreEntry.TABLE_NAME
    }
}