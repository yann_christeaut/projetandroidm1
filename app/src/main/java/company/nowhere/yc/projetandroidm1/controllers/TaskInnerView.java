package company.nowhere.yc.projetandroidm1.controllers;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.connectivity.ShareActionInnerActivity;
import company.nowhere.yc.projetandroidm1.models.Arbre;
import company.nowhere.yc.projetandroidm1.models.ArbreDBHelper;
import company.nowhere.yc.projetandroidm1.models.ArbreViewerDBHelper;
import company.nowhere.yc.projetandroidm1.models.Noeud;
import company.nowhere.yc.projetandroidm1.models.NoeudDBHelper;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;
import company.nowhere.yc.projetandroidm1.tools.Validate;

public class TaskInnerView extends AppCompatActivity {

    final int DEFAULT_VALUE = 0;

    private long idTask;
    private long idTree;
    private List<Noeud> vals;
    private AdapterItem adapter;
    private RecyclerView view;
    private SessionManager session = null;
    private Noeud noeud;
    private Arbre arbre;
    private ArbreDBHelper arbreHelper;
    private NoeudDBHelper noeudHelper;
    private DatePickerDialog dialog;
    private Menu menu;
    private RecyclerView rv;
    private RecyclerView rve;
    private User user;
    private int  y, m, d;
    private ArrayList<User> viewerToRemove = new ArrayList<User>();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        user = session.getUserDetails();
        super.onCreate(savedInstanceState);
        arbreHelper = ArbreDBHelper.Companion.getInstance(getApplicationContext());
        noeudHelper = NoeudDBHelper.Companion.getInstance(getApplicationContext());
        setContentView(R.layout.task_innerview);

        rv = findViewById(R.id.ListUser);
        rve = findViewById(R.id.ListUserEdit);
        Intent intent = getIntent();
        this.idTask = intent.getLongExtra("idTask", DEFAULT_VALUE);
        this.idTree = intent.getLongExtra("idTree", DEFAULT_VALUE);

        arbre = arbreHelper.readTree(idTree, true);
        noeud = noeudHelper.readNoeud(idTask, arbre, true, true);

        if (arbre.getRacine().getId() == idTask)
            arbre.setRacine(noeud);

        view = findViewById(R.id.Sub_item);
        vals = new ArrayList<>();
        setNode(noeud);
        vals = noeud.getEnfants();

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        view.setLayoutManager(mLayoutManager);

        adapter = new AdapterItem(vals);
        view.setAdapter(adapter);

        DateFormat df = new SimpleDateFormat("yyyy");

        y=Integer.parseInt(df.format(noeud.getDeadline()));
        df = new SimpleDateFormat("MM");
        m=Integer.parseInt(df.format(noeud.getDeadline()));
        df = new SimpleDateFormat("dd");
        d=Integer.parseInt(df.format(noeud.getDeadline()));

        dialog = new DatePickerDialog(this,null,  y, m-1, d);
        dialog.setOnDateSetListener((view, year, month, dayOfMonth) ->  {y=year;m=month;d= dayOfMonth;((TextView)findViewById(R.id.innerDeadlineEdit)).setText("Fin le : "+d+"/"+(m+1)+"/"+y);});

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.innerview, menu);
        MenuItem save = menu.findItem(R.id.inner_save_edit_icon);
        save.setVisible(false);
        this.menu = menu;
        if (!arbre.getProprietaire().is(user)){
            menu.findItem(R.id.inner_edit_icon).setVisible(false);
            menu.findItem(R.id.inner_delete_icon).setVisible(false);
            menu.findItem(R.id.inner_share_icon).setVisible(false);
            menu.findItem(R.id.inner_callback_icon).setVisible(false);
            if(!arbre.getParticipants().contains(user)){
                menu.findItem(R.id.inner_add_icon).setVisible(false);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.inner_add_icon:
                addSubTask(this.view);
                return true;
            case R.id.inner_edit_icon:
                modifyTask(this.view);
                return true;
            case R.id.inner_delete_icon:
                removeTask(this.view);
                return true;
            case R.id.inner_share_icon:
                Intent shareIntent = new Intent(this, ShareActionInnerActivity.class);
                shareIntent.putExtra("idTree", this.idTree);
                shareIntent.putExtra("idTask", this.idTask);
                startActivityForResult(shareIntent, ShareActionInnerActivity.SEND_REQUEST);
                return true;
            case R.id.inner_save_edit_icon:
                Validate();
                ((TextView)findViewById(R.id.innerDeadline)).setText("Fin le : "+d+"/"+(m+1)+"/"+y);
                resetVisibility(false);
                return true;
            case R.id.inner_callback_icon:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addSubTask(View v){
        Intent intent = new Intent(this, TaskEditItem.class);
        intent.putExtra("modify",false);
        intent.putExtra("idTask", idTask);
        intent.putExtra("idTree", idTree);

        startActivityForResult(intent, AdapterItem.REQUEST_CODE_SUB_TASK);
    }

    private void resetVisibility(boolean isEdit){
        LinearLayout show = findViewById(R.id.innerShowLayout);
        LinearLayout edit = findViewById(R.id.innerEditLayout);

        if(isEdit){
            edit.setVisibility(View.VISIBLE);
            show.setVisibility(View.GONE);

            menu.findItem(R.id.inner_add_icon).setVisible(false);
            menu.findItem(R.id.inner_edit_icon).setVisible(false);
            menu.findItem(R.id.inner_delete_icon).setVisible(false);
            menu.findItem(R.id.inner_share_icon).setVisible(false);
            menu.findItem(R.id.inner_callback_icon).setVisible(false);
            menu.findItem(R.id.inner_save_edit_icon).setVisible(true);

        }
        else{
            edit.setVisibility(View.GONE);
            show.setVisibility(View.VISIBLE);


            menu.findItem(R.id.inner_add_icon).setVisible(true);
            menu.findItem(R.id.inner_edit_icon).setVisible(true);
            menu.findItem(R.id.inner_delete_icon).setVisible(true);
            menu.findItem(R.id.inner_share_icon).setVisible(true);
            menu.findItem(R.id.inner_callback_icon).setVisible(false);
            menu.findItem(R.id.inner_save_edit_icon).setVisible(false);
        }
    }

    public void modifyTask(View v){
        resetVisibility(true);

        Noeud n = noeud;

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rve.setLayoutManager(mLayoutManager);

        AdapterUser adapterUser= new AdapterUser(arbre,v,false);
        rve.setAdapter(adapterUser);

        ((EditText)findViewById(R.id.innerTitleEdit)).setText(n.getTitre());
        ((EditText)findViewById(R.id.innerDescriptionEdit)).setText(n.getDescription());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        ((TextView)findViewById(R.id.innerDeadlineEdit)).setText("Fin le : "+dateFormat.format(n.getDeadline()));
        ((TextView) findViewById(R.id.innerLocationEdit)).setText(noeud.getLieu());

        if(noeud.getAvancement() == Noeud.FINISHED){
            ((SwitchCompat) findViewById(R.id.innerProgressToggle)).setChecked(true);
        }
        ((ProgressBar)findViewById(R.id.innerProgress)).setMax(100);
        ((ProgressBar)findViewById(R.id.innerProgress)).setProgress(noeud.getAvancement());

        switch (noeud.getPriorite()) {
            case Noeud.LOW_PRIORITY:
                ((RadioButton) findViewById(R.id.PrioBase)).setChecked(true);
                break;
            case Noeud.MEDIUM_PRIORITY:
                ((RadioButton) findViewById(R.id.PrioMoy)).setChecked(true);
                break;
            case Noeud.HIGH_PRIORITY:
                ((RadioButton) findViewById(R.id.PrioHaute)).setChecked(true);
                break;
        }

        ((ImageView) findViewById(R.id.innerDeadlineEditBtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

    }

    public void removeTask(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Êtes-vous sûr de vouloir supprimer cette tache ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Oui", (dialog, which) -> {
            if (noeud.isRacine()){
                arbreHelper.deleteTree(noeud.getClosestUpperTree(), true);
            }else {
                noeudHelper.deleteNoeud(noeud);
            }
            Intent intent = new Intent();
            setResult(AdapterItem.RESULT_CODE_UPDATED, intent);
            finish();
        });
        builder.setNegativeButton("Non", (dialog, which) -> dialog.dismiss());
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode == AdapterItem.REQUEST_CODE_SUB_TASK &&
                resultCode == AdapterItem.RESULT_CODE_UPDATED){
            vals = new ArrayList<>();
            arbre = arbreHelper.readTree(idTree, true);
            noeud = noeudHelper.readNoeud(idTask, arbre, true, false);
            if (arbre.getRacine().getId() == idTask)
                arbre.setRacine(noeud);

            vals.addAll(noeud.getEnfants());

            adapter = new AdapterItem(vals);
            ((RecyclerView) findViewById(R.id.Sub_item)).setAdapter(adapter);
        }

        if(requestCode == 2){
            noeud = noeudHelper.readNoeud(noeud.getId(), noeud.getClosestUpperTree(), true, false);
            setNode(noeud);

        }
        if (requestCode == ShareActionInnerActivity.SEND_REQUEST &&
                resultCode == ShareActionInnerActivity.SENT){
            arbre = arbreHelper.readTree(idTree, true);
            AdapterUser adapterUser= new AdapterUser(arbre,this.view, true);
            rv.setAdapter(adapterUser);
        }
    }

    protected void setNode(Noeud noeud){
        ((TextView) findViewById(R.id.innerTitle)).setText(noeud.getTitre());
        ((TextView) findViewById(R.id.innerDescription)).setText(noeud.getDescription());
        ((TextView) findViewById(R.id.innerLocation)).setText(noeud.getLieu());

        ImageView prio = (ImageView) findViewById(R.id.innerPrio);
        switch (noeud.getPriorite()) {
            case Noeud.LOW_PRIORITY:
                prio.setColorFilter(getResources().getColor(R.color.btn_add));
                break;
            case Noeud.MEDIUM_PRIORITY:
                prio.setColorFilter(getResources().getColor(R.color.lightOrange));
                break;
            case Noeud.HIGH_PRIORITY:
                prio.setColorFilter(getResources().getColor(R.color.lightRed));
                break;
        }

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        ((ProgressBar)findViewById(R.id.innerProgress)).setMax(100);
        ((ProgressBar)findViewById(R.id.innerProgress)).setProgress(noeud.getAvancement());

//        ((TextView) findViewById(R.id.innerDateBegin)).setText(df.format(noeud.getCreation()));
        ((TextView) findViewById(R.id.innerDeadline)).setText("Fin le : "+df.format(noeud.getDeadline()));

        LinearLayoutManager layoutUser = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rv.setLayoutManager(layoutUser);
        AdapterUser adapterUser= new AdapterUser(arbre,this.view, true);
        rv.setAdapter(adapterUser);

        vals = noeud.getEnfants();

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        view.setLayoutManager(mLayoutManager);

        adapter = new AdapterItem(vals);
        view.setAdapter(adapter);

    }
    public void Validate(){
        boolean insetOk = true;

        String title = ((EditText)findViewById(R.id.innerTitleEdit)).getText().toString();
        String description = ((EditText)findViewById(R.id.innerDescriptionEdit)).getText().toString();
        String place = ((EditText) findViewById(R.id.innerLocationEdit)).getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        int idRadioButtonChecked = ((RadioGroup) findViewById(R.id.PrioGroup)).getCheckedRadioButtonId();
        int priority = Noeud.HIGH_PRIORITY;
        switch (idRadioButtonChecked) {
            case R.id.PrioBase:
                priority = Noeud.LOW_PRIORITY;
                break;
            case R.id.PrioMoy:
                priority = Noeud.MEDIUM_PRIORITY;
                break;
            case R.id.PrioHaute:
                priority = Noeud.HIGH_PRIORITY;
                break;
        }

        noeud.setLieu(place);
        noeud.setTitre(title);
        noeud.setDescription(description);
        noeud.setPriorite(priority);
        try {
            noeud.setDeadline(dateFormat.parse((d+"/"+(m+1)+"/"+y)));
        }
        catch (Exception e){}

        if(((SwitchCompat)findViewById(R.id.innerProgressToggle)).isChecked()){
            noeud.setAvancement(Noeud.FINISHED);
        }
        else{
            noeud.setAvancement(0);
            noeud.setAvancement(noeud.getAvancement());
        }

        noeudHelper.updateNoeud(noeud);
        setNode(noeud);
        removeViewerToRemove();
        arbre = arbreHelper.readTree(idTree, true);
        AdapterUser adapterUser= new AdapterUser(arbre,this.view, true);
        rv.setAdapter(adapterUser);


    }
    public void addViewerToRemoveToList(User u){
        viewerToRemove.add(u);
    }
    public void removeViewerToRemoveFromList(User u){
        viewerToRemove.remove(u);
    }
    private void removeViewerToRemove(){
        ArbreViewerDBHelper viewerDBHelper = ArbreViewerDBHelper.Companion.getInstance(this);
        for(int i = 0; i< viewerToRemove.size();i++){
            viewerDBHelper.deleteViewer(arbre,viewerToRemove.get(i));
        }
        viewerToRemove = new ArrayList<User>();
    }

    @Override
    public void onResume(){
        super.onResume();
//        Toast.makeText(this,"resume",Toast.LENGTH_SHORT).show();
        noeud = noeudHelper.readNoeud(idTask, arbre, true, true);
        setNode(noeud);
    }

    @Override
    public void onRestart(){
        super.onRestart();
//        Toast.makeText(this,"restart",Toast.LENGTH_SHORT).show();
        noeud = noeudHelper.readNoeud(idTask, arbre, true, true);
        setNode(noeud);
    }
}