package company.nowhere.yc.projetandroidm1.controllers;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.models.Arbre;
import company.nowhere.yc.projetandroidm1.models.ArbreDBHelper;
import company.nowhere.yc.projetandroidm1.models.Noeud;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;

public class TaskOverview extends AppCompatActivity {

    private List<Noeud> vals;
    private RecyclerView view;
    private AdapterItem adapter;
    private ArbreDBHelper arbreBD;
    private SessionManager session = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        super.onCreate(savedInstanceState);
        arbreBD = ArbreDBHelper.Companion.getInstance(getApplicationContext());
        setContentView(R.layout.task_overview);

        view = findViewById(R.id.main_item_list);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        view.setLayoutManager(mLayoutManager);

        vals = new ArrayList<>();

        User user = session.getUserDetails();
        ArrayList<Arbre> arbres = arbreBD.readTrees(user.getId());

        for(Arbre arbre: arbres){
            vals.add(arbre.getRacine());
        }

        adapter = new AdapterItem( vals);
        view.setAdapter(adapter);

        FloatingActionButton addBtn = findViewById(R.id.new_item_btn);
        addBtn.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), TaskEditItem.class);
            startActivityForResult(intent, AdapterItem.REQUEST_CODE_SUB_TASK);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode == AdapterItem.REQUEST_CODE_SUB_TASK){

            vals = new ArrayList<>();
            User user = session.getUserDetails();
            ArrayList<Arbre> arbres = arbreBD.readTrees(user.getId());

            for(int i = 0; i<arbres.size();i++){
                vals.add(arbres.get(i).getRacine());
            }
            Toast.makeText(this,"size : "+arbres.size(),Toast.LENGTH_LONG).show();
            adapter = new AdapterItem(vals);
            ((RecyclerView) findViewById(R.id.main_item_list)).setAdapter(adapter);
        }
    }
}
