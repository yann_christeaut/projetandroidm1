package company.nowhere.yc.projetandroidm1.tools;

import java.util.regex.Pattern;

public class Validate {
    private Validate(){}
    private static final Pattern regexMail = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern regexPhone = Pattern.compile("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}");
    private static final Pattern regexSafe = Pattern.compile("[-.\\+*?\\[^\\]$(){}=!<>|:\\\\]");
    private static final Pattern regexDate = Pattern.compile("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)(?:0?2|(?:Feb))\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$");

    public static boolean isEmail(String mail){
        return isSafe(mail) && regexMail.matcher(mail).matches();
    }

    public static boolean isPhone(String phone){
        return isSafe(phone) && regexPhone.matcher(phone).matches();
    }

    public static boolean isSafe(String str){
        return !str.isEmpty() && !regexSafe.matcher(str).matches();
    }

    public static boolean isDate(String date){
        return isSafe(date) && regexDate.matcher(date).matches();
    }
}
