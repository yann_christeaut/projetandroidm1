package company.nowhere.yc.projetandroidm1.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/*
    Model de l'arbre

 */
public class Arbre implements Parcelable {
    private long id;
    private Noeud racine;
    final private User proprietaire;
    private ArrayList<User> participants;
    private ArrayList<User> viewers;
    private boolean isSubTree;

    /*
    Constructeur d'un arbre deja dans la BD
    @param id : int id dans la BD
    @param root : Noeud tache principal de l'arbre
    @param proprio : User utilisateur qui a créer l'arbre
    @param participants : ArrayList<User> liste d'utilisateur qui peuvent modifier l'arbre de tache
    @param viewers : ArrayList<User> liste d'utilisateur qui peuvent voir l'arbre de tache
     */
    public Arbre(long id, Noeud root, User proprio,
                 ArrayList<User> participants,
                 ArrayList<User> viewers,
                 boolean isSubTree){
        this.id = id;
        this.racine = root;
        this.proprietaire = proprio;
        this.participants = participants;
        this.viewers = viewers;
        this.isSubTree = isSubTree;
    }
    /*
    Constructeur d'un arbre avant l'insertion dans la BD
    @param root : Noeud tache principal de l'arbre
    @param proprio : User utilisateur qui a créer l'arbre
    @param participants : ArrayList<User> liste d'utilisateur qui peuvent modifier l'arbre de tache
    @param viewers : ArrayList<User> liste d'utilisateur qui peuvent voir l'arbre de tache
     */
    public Arbre(Noeud root, User proprio,
                 boolean isSubTree){
        this.racine = root;
        this.racine.setClosestUpperTree(this);
        this.proprietaire = proprio;
        this.participants = new ArrayList<>();
        this.viewers = new ArrayList<>();
        this.isSubTree = isSubTree;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(id);
        out.writeByte((byte) (isSubTree ? 1 : 0));
        if (isSubTree)
            out.writeParcelable(this.racine, flags);
        out.writeParcelable(this.proprietaire, flags);
        out.writeTypedList(this.participants);
        out.writeTypedList(this.viewers);
    }

    private Arbre(Parcel in) {
        this.id = in.readLong();
        this.isSubTree = in.readByte() != 0;
        if (this.isSubTree)
            this.racine = in.readParcelable(Noeud.class.getClassLoader());
        this.proprietaire = in.readParcelable(User.class.getClassLoader());
        this.participants = in.createTypedArrayList(User.CREATOR);
        this.viewers = in.createTypedArrayList(User.CREATOR);
    }

    public static final Creator<Arbre> CREATOR = new Creator<Arbre>() {
        @Override
        public Arbre createFromParcel(Parcel in) {
            return new Arbre(in);
        }

        @Override
        public Arbre[] newArray(int size) {
            return new Arbre[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Noeud getRacine(){
        return racine;
    }

    public void setRacine(Noeud racine){ this.racine = racine; }

    public User getProprietaire() {
        return proprietaire;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public boolean isSubTree() {
        return isSubTree;
    }

    public void addParticipant(User participant){
        this.participants.add(participant);
    }

    public void addParticipants(ArrayList<User> participant) {
        this.viewers.addAll(viewers);
    }

    public ArrayList<User> getViewers() {
        return viewers;
    }

    public void addViewer(User participant){
        this.participants.add(participant);
    }

    public void addViewers(ArrayList<User> viewers) {
        this.viewers.addAll(viewers);
    }
}
