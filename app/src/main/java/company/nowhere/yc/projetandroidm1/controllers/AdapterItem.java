package company.nowhere.yc.projetandroidm1.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.models.Noeud;
import company.nowhere.yc.projetandroidm1.models.NoeudDBHelper;


public class AdapterItem extends RecyclerView.Adapter<AdapterItem.MyViewHolder> {
    private List<Noeud> mDataset;
    private Context mContext;

    final static int REQUEST_CODE_SUB_TASK = 1;
    final static int REQUEST_CODE_DATA = 2;

    final static int RESULT_CODE_UPDATED = 1;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    // Provide a suitable constructor (depends on the kind of dataset)
    AdapterItem(List<Noeud> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public AdapterItem.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_overview_item, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //  Object task = mDataset.get(position);

        NoeudDBHelper noeudDBHelper = NoeudDBHelper.Companion.getInstance(mContext);
        Noeud noeud = noeudDBHelper.readNoeud(mDataset.get(position).getId(), mDataset.get(position).getClosestUpperTree(), true, true);
        holder.setVal(noeud);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        private Noeud noeud;
        private int toggleState = 0;
        MyViewHolder(final View itemView) {

            super(itemView);

            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), TaskInnerView.class);
                intent.putExtra("idTask", noeud.getId());
                intent.putExtra("idTree", noeud.getClosestUpperTree().getId());
                ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SUB_TASK);
            });

        }

        void setVal(Noeud noeud){
            this.noeud = noeud;

            if(noeud!=null) {
                ((TextView) itemView.findViewById(R.id.prefab_title)).setText(noeud.getTitre());
                ((TextView) itemView.findViewById(R.id.prefab_description)).setText(noeud.getDescription());

//                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                long date = TimeUnit.DAYS.convert(noeud.getDeadline().getTime()-noeud.getCreation().getTime(), TimeUnit.MILLISECONDS);
                ((TextView) itemView.findViewById(R.id.prefab_deadline)).setText(date+" J");
                ((ProgressBar) itemView.findViewById(R.id.prefab_progress)).setMax(100);
                ((ProgressBar) itemView.findViewById(R.id.prefab_progress)).setProgress(noeud.getAvancement());
                ImageView prio = (ImageView) itemView.findViewById(R.id.prefab_priority);
                switch (noeud.getPriorite()) {
                    case Noeud.LOW_PRIORITY:
                        prio.setColorFilter(itemView.getResources().getColor(R.color.btn_add));
                        break;
                    case Noeud.MEDIUM_PRIORITY:
                        prio.setColorFilter(itemView.getResources().getColor(R.color.lightOrange));
                        break;
                    case Noeud.HIGH_PRIORITY:
                        prio.setColorFilter(itemView.getResources().getColor(R.color.lightRed));
                        break;
                }


            }
            ((Button) itemView.findViewById(R.id.prefab_toogle)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleClick(v);
                }
            });
        }
        public void toggleClick(View v){
            Button btn = ((Button) v.findViewById(R.id.prefab_toogle));
            switch(toggleState){
                case 1:
                    btn.setBackground(ContextCompat.getDrawable(v.getContext(), R.drawable.expand_less));
                    ((TextView) itemView.findViewById(R.id.prefab_description)).setVisibility(View.VISIBLE);
                    break;
                case 0 :
                    btn.setBackground(ContextCompat.getDrawable(v.getContext(), R.drawable.expand_more));
                    ((TextView) itemView.findViewById(R.id.prefab_description)).setVisibility(View.GONE);
                    break;
            }
            toggleState=1-toggleState;
        }
    }
}