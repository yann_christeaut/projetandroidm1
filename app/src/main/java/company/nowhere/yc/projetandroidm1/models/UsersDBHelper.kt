package company.nowhere.yc.projetandroidm1.models

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import android.provider.BaseColumns
import android.util.Log
import company.nowhere.yc.projetandroidm1.database.DBContract
import company.nowhere.yc.projetandroidm1.database.DBHelper
import company.nowhere.yc.projetandroidm1.tools.Singleton

/*
    Gere tous les requete sur la table utilisateur dans la BD
    @param DBHelper : SQLiteOpenHelper acces a la BD
 */
class UsersDBHelper private constructor(context : Context) {

    private val helper = DBHelper.getInstance(context)

    /*
    Insere un utilisateur dans la BD
    @param user : User
     */
    @Throws(SQLiteConstraintException::class)
    fun insertUser(user: User, deviceId: String): Boolean {
        // Gets the data repository in write mode
        val db = helper.writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(DBContract.UserEntry.COLUMN_NOM, user.nom)
        values.put(DBContract.UserEntry.COLUMN_PRENOM, user.prenom)
        values.put(DBContract.UserEntry.COLUMN_MAIL, user.mail)
        values.put(DBContract.UserEntry.COLUMN_PHONE, user.telephone)
        values.put(DBContract.UserEntry.COLUMN_USERNAME, user.username)
        values.put(DBContract.UserEntry.COLUMN_PASSWD, user.password)

        var inserted = true
        // Insert the new row, returning the primary key value of the new row
        try {
            user.id = db.insertOrThrow(DBContract.UserEntry.TABLE_NAME, null, values)
        }catch(e : SQLException){
            inserted = false
        }finally {
            db.close()
        }
        if (inserted) {
            insertDeviceUser(user, deviceId)
        }
        return inserted
    }

    /*
    Insere un device pour un utilisateur dans la BD
    @param user : User
     */
    @Throws(SQLiteConstraintException::class)
    fun insertDeviceUser(user: User, device: String) {
        val db = helper.writableDatabase
        val values = ContentValues()
        values.put(DBContract.DeviceEntry.COLUMN_ID_USER, user.id)
        values.put(DBContract.DeviceEntry.COLUMN_ID_DEVICE, device)
        db.insertOrThrow(DBContract.DeviceEntry.TABLE_NAME, null, values)
        db.close()
    }
    /*
    Lis un utilisateur dans la BD
    @param userId : Int
    @return User
     */
    fun readUser(userId: Long): User? {
        var user : User? = null
        val db = helper.readableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("SELECT * FROM " + DBContract.UserEntry.TABLE_NAME +
                                    " WHERE " + BaseColumns._ID + "='" + userId + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES_USER)
            return null
        }

        val nom: String
        val prenom: String
        val mail : String
        val phone : String
        val username : String
        val passwd : String

        if (cursor!!.moveToFirst()) {
            nom = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_NOM))
            prenom = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PRENOM))
            mail = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_MAIL))
            phone = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PHONE))
            username = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_USERNAME))
            passwd = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PASSWD))
            user = User(userId, nom, prenom, mail, username, passwd, phone)
        }
        cursor.close()
        db.close()
        return user
    }

    /*
    Lis un utilisateur dans la BD par son username
    @param userId : Int
    @return User
     */
    fun readUserConnect(userName: String, passwd : String, deviceId : String): User? {
        var user : User? = null
        val db = helper.readableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery( "SELECT * FROM " + DBContract.UserEntry.TABLE_NAME +
                                    " WHERE " + DBContract.UserEntry.COLUMN_USERNAME + "='" + userName + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES_USER)
            return null
        }
        val id : Long
        val nom: String
        val prenom: String
        val mail : String
        val phone : String
        val passwdBD : String
        if (cursor!!.moveToFirst()) {
            passwdBD = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PASSWD))
            if (passwdBD == passwd) {
                nom = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_NOM))
                prenom = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PRENOM))
                mail = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_MAIL))
                phone = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PHONE))
                id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID))
                user = User(id, nom, prenom, mail, userName, passwd, phone)
                isDeviceRegistered(user, deviceId)
            }
        }
        cursor.close()
        db.close()
        return user
    }

    /*
    Lis un utilisateur dans la BD par son username
    @param userId : Int
    @return User
     */
    fun readUserToShare(userName: String): User? {
        var user : User? = null
        val db = helper.readableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery( "SELECT * FROM " + DBContract.UserEntry.TABLE_NAME +
                    " WHERE " + DBContract.UserEntry.COLUMN_USERNAME + "='" + userName + "'", null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES_USER)
            return null
        }
        val id : Long
        val nom: String
        val prenom: String
        val mail : String
        val phone : String
        if (cursor!!.moveToFirst()) {
                nom = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_NOM))
                prenom = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PRENOM))
                mail = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_MAIL))
                phone = cursor.getString(cursor.getColumnIndex(DBContract.UserEntry.COLUMN_PHONE))
                id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID))
                user = User(id, nom, prenom, mail, userName, "", phone)
        }
        cursor.close()
        db.close()
        return user
    }

    private fun isDeviceRegistered(user : User, deviceId : String){
        val db = helper.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery( "SELECT * FROM " + DBContract.DeviceEntry.TABLE_NAME +
                                    " WHERE " + DBContract.DeviceEntry.COLUMN_ID_DEVICE + "='" + deviceId + "'", null)
        } catch (e: SQLiteException) {
            Log.d("DATABASE","Fail to read device table")
        }
        if (cursor!!.columnCount == 0) {
            insertDeviceUser(user, deviceId)
        }
        cursor.close()
        db.close()
    }

    companion object : Singleton<UsersDBHelper, Context>(::UsersDBHelper) {

        const val SQL_CREATE_ENTRIES_USER =
                "CREATE TABLE ${DBContract.UserEntry.TABLE_NAME} (" +
                        "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                        "${DBContract.UserEntry.COLUMN_NOM} TEXT NOT NULL," +
                        "${DBContract.UserEntry.COLUMN_PRENOM} TEXT NOT NULL," +
                        "${DBContract.UserEntry.COLUMN_MAIL} TEXT NOT NULL," +
                        "${DBContract.UserEntry.COLUMN_PHONE} TEXT NOT NULL," +
                        "${DBContract.UserEntry.COLUMN_USERNAME} TEXT NOT NULL," +
                        "${DBContract.UserEntry.COLUMN_PASSWD} TEXT NOT NULL," +
                        "CONSTRAINT ${DBContract.UserEntry.COLUMN_USERNAME}_unique UNIQUE (${DBContract.UserEntry.COLUMN_USERNAME}))"

        const val SQL_CREATE_ENTRIES_DEVICE =
                "CREATE TABLE ${DBContract.DeviceEntry.TABLE_NAME} (" +
                        "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                        "${DBContract.DeviceEntry.COLUMN_ID_DEVICE} TEXT NOT NULL," +
                        "${DBContract.DeviceEntry.COLUMN_ID_USER} INTEGER NOT NULL," +
                        "FOREIGN KEY(${DBContract.DeviceEntry.COLUMN_ID_USER}) " +
                        "REFERENCES ${DBContract.UserEntry.TABLE_NAME}(${BaseColumns._ID}) ON DELETE CASCADE)"

        const val SQL_DELETE_ENTRIES_USER = "DROP TABLE IF EXISTS " + DBContract.UserEntry.TABLE_NAME

        const val SQL_DELETE_ENTRIES_DEVICE = "DROP TABLE IF EXISTS " + DBContract.DeviceEntry.TABLE_NAME
    }
}