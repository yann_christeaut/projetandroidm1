package company.nowhere.yc.projetandroidm1.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.security.NoSuchAlgorithmException;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.models.UsersDBHelper;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;
import company.nowhere.yc.projetandroidm1.tools.Validate;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
    }

    public void Register(View v) throws NoSuchAlgorithmException{
        EditText userField = findViewById(R.id.reg_username);
        String username = userField.getText().toString();
        EditText passwdField = findViewById(R.id.reg_passwd);
        String passwd = passwdField.getText().toString();
        EditText nomField = findViewById(R.id.reg_nom);
        String nom = nomField.getText().toString();
        EditText prenomField = findViewById(R.id.reg_prenom);
        String prenom = prenomField.getText().toString();
        EditText mailField = findViewById(R.id.reg_mail);
        String mail = mailField.getText().toString();
        EditText phoneField = findViewById(R.id.reg_phone);
        String phone = phoneField.getText().toString();
        if (Validate.isSafe(username) && Validate.isSafe(passwd)
                && Validate.isSafe(nom) && Validate.isSafe(prenom)
                && Validate.isEmail(mail) && Validate.isPhone(phone)){
            UsersDBHelper userDB = UsersDBHelper.Companion.getInstance(getApplicationContext());
            User user = new User(nom, prenom, mail, username, passwd, phone);
            String uniqId = SessionManager.getUniqueId(getApplicationContext());
            boolean success = userDB.insertUser(user, uniqId);
            if (success) {
                SessionManager session = new SessionManager(getApplicationContext());
                session.createLoginSession(user);
                Intent intent = new Intent(this, TaskOverview.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else {
                userField.setError("Le nom d'utilisateur existe deja.");
            }
        }
        if (!Validate.isSafe(username)){
            showInvalidUser(userField, "nom d'utilisateur");
        }
        if (!Validate.isSafe(passwd)){
            showInvalidUser(passwdField, "mot de passe");
        }
        if (!Validate.isSafe(prenom)){
            showInvalidUser(prenomField, "prenom");
        }
        if (!Validate.isSafe(nom)){
            showInvalidUser(nomField, "nom");
        }
        if (!Validate.isSafe(phone)){
            showInvalidUser(phoneField, "telephone");
        }
        if (!Validate.isSafe(mail)){
            showInvalidUser(mailField, "mail");
        }
    }

    private void showInvalidUser(EditText field, String msg){
        field.setError("Le "+msg+" n'est pas valide.");
    }
}
