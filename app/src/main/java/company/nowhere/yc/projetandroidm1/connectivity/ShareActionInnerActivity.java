package company.nowhere.yc.projetandroidm1.connectivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.models.Arbre;
import company.nowhere.yc.projetandroidm1.models.ArbreDBHelper;
import company.nowhere.yc.projetandroidm1.models.ArbreViewerDBHelper;
import company.nowhere.yc.projetandroidm1.models.Noeud;
import company.nowhere.yc.projetandroidm1.models.NoeudDBHelper;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.models.UsersDBHelper;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;

public class ShareActionInnerActivity extends AppCompatActivity {
    public final static int SEND_REQUEST = 10;
    public final static int SENT = 11;
    private final int DEFAULT_VALUE = 0;
    private SessionManager session;
    private long idTask;
    private long idTree;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        Intent intent = getIntent();
        this.idTask = intent.getLongExtra("idTask", DEFAULT_VALUE);
        this.idTree = intent.getLongExtra("idTree", DEFAULT_VALUE);
        setContentView(R.layout.share_actions);
        (findViewById(R.id.shareActionValid)).setOnClickListener(v -> send());
    }

    private void send(){
        EditText username = findViewById(R.id.username_share);
        String userToName = username.getText().toString();
        // we should use an api to resolve this as the db is shared it's ok for project
        User to = UsersDBHelper.Companion.getInstance(getApplicationContext()).readUserToShare(userToName);
        if (to != null) {
            int idRadioButtonChecked = ((RadioGroup) findViewById(R.id.sendWay)).getCheckedRadioButtonId();
            boolean success = false;
            switch (idRadioButtonChecked) {
                case R.id.sendByMail:
                    success = sendMail(to);
                    break;
                case R.id.sendByPhone:
                    success = sendPhone(to);
                    break;
                case R.id.sendByBluetooth:
                    success = sendBluetooth(to);
                    break;
            }
            if (success) {
                int idBtnMayModify = ((RadioGroup) findViewById(R.id.share_choose_modif)).getCheckedRadioButtonId();
                boolean mayModify = idBtnMayModify == R.id.share_may_change;
                Arbre oldArbre = ArbreDBHelper.Companion.getInstance(getApplicationContext()).readTree(this.idTree, true);
                long toInsert = this.idTree;
                if (oldArbre.getRacine().getId() != this.idTask) {
                    Noeud oldNoeud = NoeudDBHelper.Companion.getInstance(getApplicationContext()).readNoeud(idTask, oldArbre, false, false);
                    Arbre nArbre = new Arbre(oldNoeud, oldArbre.getProprietaire(), true);
                    ArbreDBHelper.Companion.getInstance(getApplicationContext()).insertTree(nArbre);
                    toInsert = nArbre.getId();
                }
                ArbreViewerDBHelper.Companion.getInstance(getApplicationContext()).insertViewer(to, mayModify, toInsert);
                setResult(SENT);
                finish();
            }
        }else {
            username.setError("Le nom d'utilisateur n'existe pas.");
        }
    }

    private boolean sendPhone(User to){
        Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + to.getTelephone() ) );
        startActivity(Intent.createChooser(intent, "Send sms..."));
        return true;
    }

    private boolean sendMail(User to){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{to.getMail()});
        boolean success = true;
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "Le partage a échoué.", Toast.LENGTH_SHORT).show();
            success = false;
        }
        return success;
    }

    private boolean sendBluetooth(User to){
        return true;
    }
}
