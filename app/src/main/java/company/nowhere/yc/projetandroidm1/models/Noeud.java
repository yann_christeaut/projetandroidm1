package company.nowhere.yc.projetandroidm1.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.ArrayList;

/*
    Model d'une tache
 */
public class Noeud implements Parcelable {

    public final static int NOT_FINISHED = 0;
    public final static int FINISHED = 100;
    public final static int LOW_PRIORITY = 1;
    public final static int MEDIUM_PRIORITY = 2;
    public final static int HIGH_PRIORITY = 3;

    private long id;
    private String titre;
    private String description;
    private String lieu;
    final private Date creation;
    private Date deadline;
    private Noeud parent;
    final private ArrayList<Noeud> enfants;
    private Arbre upperTree;
    private int priorite;
    private int avancement;
    private boolean isSubTree;
    private User assignee;

    /*
        @param id : int id dans la BD
        @param titre : String du titre de la tache
        @param desc : String description d'une tache
        @param lieu : String representant l'adresse d'une tache
        @param creat : Date de creation d'une tache
        @param dead : Date deadline de la tache
        @param upperTree : Arbre le plus proche pour connaitre les participants
        @param parent : Noeud parent de la tache / ou null si racine
        @param avancement : int E 0..100 representant l'avancement d'une tache
        @param priorite : int E 1..4 representant la priorite d'une tache
        @param isSubTree : boolean pour savoir si cette tache est aussi racine
     */
    public Noeud(long id, String titre, String desc,
                 String lieu, Date creat, Date dead,
                 Arbre upperTree, Noeud parent,
                 int avancement, int priorite, boolean isSubTree,
                 User assignee){
        this.id = id;
        this.titre = titre;
        this.description = desc;
        this.lieu = lieu;
        this.creation = creat;
        this.deadline = dead;
        this.parent = parent;
        this.enfants = new ArrayList<>();
        this.avancement = avancement;
        this.priorite = priorite;
        this.upperTree = upperTree;
        this.isSubTree = isSubTree;
        this.assignee = assignee;
    }

    /*
        @param titre : String du titre de la tache
        @param desc : String description d'une tache
        @param lieu : String representant l'adresse d'une tache
        @param creat : Date de creation d'une tache
        @param dead : Date deadline de la tache
        @param upperTree : Arbre le plus proche pour connaitre les participants
        @param parent : Noeud parent de la tache / ou null si racine
        @param avancement : int E 0..100 representant l'avancement d'une tache
        @param priorite : int E 1..4 representant la priorite d'une tache
        @param isSubTree : boolean pour savoir si cette tache est aussi racine
     */
    public Noeud(String titre, String desc, String lieu,
                 Date creat, Date dead, Arbre upperTree,
                 Noeud parent, int avancement, int priorite,
                 boolean isSubTree, User assignee){
        this.titre = titre;
        this.description = desc;
        this.lieu = lieu;
        this.creation = creat;
        this.deadline = dead;
        this.parent = parent;
        this.enfants = new ArrayList<>();
        this.avancement = avancement;
        this.priorite = priorite;
        this.upperTree = upperTree;
        this.isSubTree = isSubTree;
        this.assignee = assignee;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(titre);
        dest.writeString(description);
        dest.writeString(lieu);
        dest.writeLong(creation.getTime());
        dest.writeLong(deadline.getTime());
        dest.writeTypedList(enfants);
        dest.writeParcelable(upperTree, flags);
        dest.writeInt(priorite);
        dest.writeInt(avancement);
        dest.writeByte((byte) (isSubTree ? 1 : 0));
        dest.writeParcelable(assignee, flags);
    }

    private Noeud(Parcel in) {
        id = in.readLong();
        titre = in.readString();
        description = in.readString();
        lieu = in.readString();
        creation = new Date(in.readLong());
        deadline = new Date(in.readLong());
        enfants = in.createTypedArrayList(Noeud.CREATOR);
        priorite = in.readInt();
        avancement = in.readInt();
        isSubTree = in.readByte() != 0;
        assignee = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Noeud> CREATOR = new Creator<Noeud>() {
        @Override
        public Noeud createFromParcel(Parcel in) {
            return new Noeud(in);
        }

        @Override
        public Noeud[] newArray(int size) {
            return new Noeud[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id){ this.id = id; }

    public Date getCreation() {
        return creation;
    }

    public Date getDeadline() {
        return deadline;
    }

    public boolean isSubTree() {
        return isSubTree;
    }

    public void setSubTree(boolean subTree) {
        isSubTree = subTree;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    /*
    @param user : User actuel pour savoir s'il peut voir au dessus de ce noeud par rapport au viewers
    @return Noeud : parent de ce noeud
     */
    public Noeud getParent(){
        return this.parent;
    }

    public boolean isRacine() { return this.isSubTree || this == this.getClosestUpperTree().getRacine(); }

    public Arbre getClosestUpperTree() {
        return upperTree;
    }

    public void setClosestUpperTree(Arbre tree){
        this.upperTree = tree;
    }

    public String getLieu() { return lieu; }

    public void setLieu(String lieu) { this.lieu = lieu; }

    public int getPriorite() { return priorite; }

    public void setPriorite(int priorite) { this.priorite = priorite; }

    public ArrayList<Noeud> getEnfants() {
        return enfants;
    }

    public void addEnfant(Noeud enfant) {
        this.enfants.add(enfant);
    }

    public void addAllEnfants(ArrayList<Noeud> enfants){ this.enfants.addAll(enfants); }

    public String getTitre(){
        return titre;
    }

    public void setTitre(String titre){
        this.titre = titre;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String desc){
        this.description = desc;
    }

    public void setAvancement(int avancement){
        this.avancement = avancement;
    }

    public int getAvancement(){
        int res = 0;
        if(this.avancement!= Noeud.FINISHED){
            for(int i = 0; i< enfants.size();i++){
                res+= enfants.get(i).getAvancement()/enfants.size();
            }
        }
        else{
            res = Noeud.FINISHED;
        }

        return res;
    }

    public User getAssignee() {
        return assignee;
    }

    void setAssignee(User u){ this.assignee = u; }
}
