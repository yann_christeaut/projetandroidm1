package company.nowhere.yc.projetandroidm1.models

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import android.provider.BaseColumns
import android.util.Log
import company.nowhere.yc.projetandroidm1.database.DBContract
import company.nowhere.yc.projetandroidm1.database.DBHelper
import company.nowhere.yc.projetandroidm1.tools.Singleton
import java.sql.Date
import java.util.*
import kotlin.collections.ArrayList

/*
    Gere toutes les interactions avec la table Noeud dans la BD
    @param DBHelper : SQLiteOpenHelper acces a la BD
 */
open class NoeudDBHelper private constructor(context : Context) {

    private val helper = DBHelper.getInstance(context)
    private val context = context
    private val arbreHelper = ArbreDBHelper.getInstance(context)
    private val userHelper = UsersDBHelper.getInstance(context)

    /*
    Insere un noeud dans la BD
    @param noeud : Noeud a inserer
    @return true si la BD a accepter la requete
    @throws SQLiteConstraintException si la BD n'a pas les droit en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun insertNoeud(noeud: Noeud, arbre : Arbre) {
        // Gets the data repository in write mode
        val db = helper.writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(DBContract.NoeudEntry.COLUMN_TITRE, noeud.titre)
        values.put(DBContract.NoeudEntry.COLUMN_DESC, noeud.description)
        values.put(DBContract.NoeudEntry.COLUMN_LIEU, noeud.lieu)
        values.put(DBContract.NoeudEntry.COLUMN_CREATION, noeud.creation.time)
        values.put(DBContract.NoeudEntry.COLUMN_DEADLINE, noeud.deadline.time)
        values.put(DBContract.NoeudEntry.COLUMN_PRIORITY, noeud.priorite)
        values.put(DBContract.NoeudEntry.COLUMN_AVANCEMENT, noeud.avancement)
        values.put(DBContract.NoeudEntry.COLUMN_IS_SUB_TREE, noeud.isSubTree)
        values.put(DBContract.NoeudEntry.COLUMN_ID_TREE, arbre.id)
        if (noeud.parent != null)
            values.put(DBContract.NoeudEntry.COLUMN_ID_PERE, noeud.parent.id)

        // Insert the new row, returning the primary key value of the new row
        noeud.id = db.insert(DBContract.NoeudEntry.TABLE_NAME, null, values)
        db.close()
    }

    /*
    Supprime un noeud dans la BD
    @param noeud : Noeud a supprimer
    @return true si la BD a accepter la requete
    @throws SQLiteConstraintException si la BD n'a pas les droit en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun deleteNoeud(noeud: Noeud) {
        // Define 'where' part of query.
        val whereClause : String
        // Join ids of deleted nodes.
        var whereArgs = "("
        var current : Noeud
        if (noeud.enfants.size > 0){
            whereClause = BaseColumns._ID + " IN "
            val fifo = LinkedList<Noeud>()
            val completeNode = readNoeud(noeud.id, noeud.closestUpperTree, true, true)!!
            fifo.add(completeNode)
            while (fifo.size > 0){
                current = fifo.removeFirst()
                whereArgs += current.id.toString()
                if (current.isSubTree)
                    ArbreDBHelper.getInstance(context).deleteTree(current.closestUpperTree, false)
                fifo.addAll(current.enfants)
                whereArgs += if (fifo.size > 0) "," else  ")"
            }
        }else{
            whereClause = BaseColumns._ID + "="
            whereArgs = noeud.id.toString()
        }
        // Gets the data repository in write mode
        val db = helper.writableDatabase
        // Issue SQL statement.
        db.delete(DBContract.NoeudEntry.TABLE_NAME, whereClause + whereArgs, null)
        db.close()
    }

    /*
    Update un noeud dans la BD
    @param noeud : Noeud a mettre a jour
    @return true si la BD a accepter la requete
     */
    fun updateNoeud(noeud: Noeud) {
        val db = helper.writableDatabase
        val values = ContentValues()
        values.put(DBContract.NoeudEntry.COLUMN_TITRE, noeud.titre)
        values.put(DBContract.NoeudEntry.COLUMN_DESC, noeud.description)
        values.put(DBContract.NoeudEntry.COLUMN_LIEU, noeud.lieu)
        values.put(DBContract.NoeudEntry.COLUMN_CREATION, noeud.creation.time)
        values.put(DBContract.NoeudEntry.COLUMN_DEADLINE, noeud.deadline.time)
        values.put(DBContract.NoeudEntry.COLUMN_PRIORITY, noeud.priorite)
        values.put(DBContract.NoeudEntry.COLUMN_AVANCEMENT, noeud.avancement)
        values.put(DBContract.NoeudEntry.COLUMN_IS_SUB_TREE, noeud.isSubTree)
        db.update(DBContract.NoeudEntry.TABLE_NAME, values, "${BaseColumns._ID} = ?", arrayOf(noeud.id.toString()))
        db.close()
    }

    /*
    Lis un Noeud et ses enfants dans la BD
    @param noeudId : Int id du noeud à lire dans la BD
    @param upperTree : Arbre qui demande cette requete
    @param parent : Noeud? parent du noeud requeté
     */
    fun readNoeud(noeudId: Long, arbre: Arbre, withChildren : Boolean, complete : Boolean): Noeud? {
        var noeud : Noeud? = null
        val db = helper.readableDatabase
        val cursorNoeud: Cursor?
        try {
            cursorNoeud = db.rawQuery("SELECT * " +
                                        "FROM " + DBContract.NoeudEntry.TABLE_NAME +
                                        " WHERE " + BaseColumns._ID + "='" + noeudId+"'", null)
        } catch (e: SQLiteException) {
            db.close()
            return null
        }

        val titre : String
        val desc : String
        val lieu : String
        val creation : Date
        val deadline : Date
        val priorite : Int
        val avancement : Int
        val isSubTree : Boolean
        val upperT : Arbre?
        val assigneeId : Long
        val assignee : User?
        if (cursorNoeud!!.moveToFirst() && !cursorNoeud.isAfterLast) {
            titre = cursorNoeud.getString(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_TITRE))
            desc = cursorNoeud.getString(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_DESC))
            lieu = cursorNoeud.getString(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_LIEU))
            creation = Date(cursorNoeud.getLong(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_CREATION)))
            deadline = Date(cursorNoeud.getLong(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_DEADLINE)))
            priorite = cursorNoeud.getInt(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_PRIORITY))
            avancement = cursorNoeud.getInt(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_AVANCEMENT))
            isSubTree = cursorNoeud.getInt(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_IS_SUB_TREE)) != 0
            upperT = if (isSubTree) arbreHelper.readTreeByRoot(noeudId, arbre) else arbre
            assigneeId = cursorNoeud.getLong(cursorNoeud.getColumnIndex(DBContract.NoeudEntry.COLUMN_ID_ASSIGNEE))
            assignee = userHelper.readUser(assigneeId)
            noeud = Noeud(noeudId, titre, desc, lieu, creation, deadline,
                            upperT, null, avancement, priorite, isSubTree, assignee)
            if (isSubTree) {
                upperT?.racine = noeud
            }
        }
        cursorNoeud.close()
        db.close()
        if (withChildren){
            noeud?.addAllEnfants(readEnfants(noeudId, arbre, complete))
        }
        return noeud
    }

    private fun readEnfants(noeudId: Long, arbre: Arbre, complete: Boolean) : ArrayList<Noeud>? {
        val noeuds = ArrayList<Noeud>()
        val db = helper.readableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("SELECT * " +
                    "FROM " + DBContract.NoeudEntry.TABLE_NAME +
                    " WHERE " + DBContract.NoeudEntry.COLUMN_ID_PERE + "='" + noeudId+"'", null)
        } catch (e: SQLiteException) {
            db.close()
            return null
        }

        if (cursor!!.moveToFirst()){
            var titre: String
            var desc: String
            var lieu: String
            var creation: Date
            var deadline: Date
            var priorite: Int
            var avancement: Int
            var isSubTree: Boolean
            var upperT: Arbre?
            var enfId: Long
            var noeud : Noeud
            var assigneeId : Long
            var assignee : User?
            while (!cursor.isAfterLast) {
                enfId = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID))
                titre = cursor.getString(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_TITRE))
                desc = cursor.getString(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_DESC))
                lieu = cursor.getString(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_LIEU))
                creation = Date(cursor.getLong(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_CREATION)))
                deadline = Date(cursor.getLong(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_DEADLINE)))
                priorite = cursor.getInt(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_PRIORITY))
                avancement = cursor.getInt(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_AVANCEMENT))
                isSubTree = cursor.getInt(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_IS_SUB_TREE)) != 0
                assigneeId = cursor.getLong(cursor.getColumnIndex(DBContract.NoeudEntry.COLUMN_ID_ASSIGNEE))
                assignee = userHelper.readUser(assigneeId)
                upperT = if (isSubTree) arbreHelper.readTreeByRoot(noeudId, arbre) else arbre
                noeud = Noeud(enfId, titre, desc, lieu, creation, deadline,
                                upperT, null, avancement, priorite, isSubTree, assignee)
                if (isSubTree)
                    upperT?.racine = noeud
                if (complete)
                    noeud.addAllEnfants(readEnfants(enfId, arbre, complete))
                noeuds.add(noeud)
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()
        return noeuds
    }

    companion object : Singleton<NoeudDBHelper, Context>(::NoeudDBHelper) {

        const val SQL_CREATE_ENTRIES =
                "CREATE TABLE ${DBContract.NoeudEntry.TABLE_NAME} (" +
                        "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                        "${DBContract.NoeudEntry.COLUMN_TITRE} TEXT NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_DESC} TEXT NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_LIEU} TEXT NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_CREATION} INTEGER NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_DEADLINE} INTEGER NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_PRIORITY} INTEGER NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_AVANCEMENT} INTEGER NOT NULL," +
                        "${DBContract.NoeudEntry.COLUMN_IS_SUB_TREE} INTEGER NOT NULL,"+
                        "${DBContract.NoeudEntry.COLUMN_ID_TREE} INTEGER NOT NULL,"+
                        "${DBContract.NoeudEntry.COLUMN_ID_PERE} INTEGER," +
                        "${DBContract.NoeudEntry.COLUMN_ID_ASSIGNEE} INTEGER," +
                        "FOREIGN KEY(${DBContract.NoeudEntry.COLUMN_ID_ASSIGNEE}) " +
                        "REFERENCES ${DBContract.UserEntry.TABLE_NAME}(${BaseColumns._ID}), "+
                        "FOREIGN KEY(${DBContract.NoeudEntry.COLUMN_ID_PERE}) " +
                        "REFERENCES ${DBContract.NoeudEntry.TABLE_NAME}(${BaseColumns._ID}) ON DELETE CASCADE)"

        const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DBContract.NoeudEntry.TABLE_NAME
    }
}