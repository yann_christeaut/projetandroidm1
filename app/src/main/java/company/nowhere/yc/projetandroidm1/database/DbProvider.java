package company.nowhere.yc.projetandroidm1.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.jumpmind.symmetric.android.SQLiteOpenHelperRegistry;
import org.jumpmind.symmetric.android.SymmetricService;
import org.jumpmind.symmetric.common.ParameterConstants;

import java.util.Properties;

import company.nowhere.yc.projetandroidm1.tools.SessionManager;

public class DbProvider extends ContentProvider {

    private final String REGISTRATION_URL = "http://192.168.1.12:31415/sync/master";
    private String ENGINE_NAME = "android";
    private final String NODE_GROUP = "client";

    @Override
    public boolean onCreate() {
        Context context = getContext();
        String externalId = SessionManager.getUniqueId(context);
        DBHelper helper = DBHelper.Companion.getInstance(context);
        SQLiteOpenHelperRegistry.register(DBHelper.DATABASE_NAME, helper);
        Intent intent = new Intent(getContext(), SymmetricService.class);
        intent.putExtra(SymmetricService.INTENTKEY_SQLITEOPENHELPER_REGISTRY_KEY, DBHelper.DATABASE_NAME);
        intent.putExtra(SymmetricService.INTENTKEY_REGISTRATION_URL, REGISTRATION_URL);
        intent.putExtra(SymmetricService.INTENTKEY_EXTERNAL_ID, externalId);
        intent.putExtra(SymmetricService.INTENTKEY_NODE_GROUP_ID, NODE_GROUP);
        intent.putExtra(SymmetricService.INTENTKEY_START_IN_BACKGROUND, true);

        Properties properties = new Properties();
        properties.put(ParameterConstants.STREAM_TO_FILE_ENABLED, "true");
        properties.put(ParameterConstants.INITIAL_LOAD_USE_EXTRACT_JOB, "false");
        properties.put(ParameterConstants.ENGINE_NAME, ENGINE_NAME);
        intent.putExtra(SymmetricService.INTENTKEY_PROPERTIES, properties);

        context.startService(intent);

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri,
                      @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri,
                      @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values,
                      @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
