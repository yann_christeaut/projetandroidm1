package company.nowhere.yc.projetandroidm1.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.jar.Pack200;

/*
    Model de l'utilisateur
 */
public class User implements Parcelable {

    private long id;
    private String nom;
    private String prenom;
    private String mail;
    private String username;
    private String password;
    private String telephone;

    /*
    Constructeur d'un utilisateur depuis la BD
    @param id : int id de l'user dans la BD
    @param nom : String
    @param premom : String
    @param mail : String
    @param username : String nom d'utilisateur
    @param passwd : String mot de passe -> hashé
    @param phone : String
     */
    public User(long id, String nom, String prenom,
                String mail, String username, String passwd,
                String phone) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.username = username;
        this.password = passwd;
        this.telephone = phone;
    }

    /*
    Constructeur d'un utilisateur avant l'insert dans la BD
    @param nom : String
    @param premom : String
    @param mail : String
    @param username : String nom d'utilisateur
    @param passwd : String mot de passe -> en clair
    @param phone : String
     */
    public User(String nom, String prenom,
                String mail, String username, String passwd,
                String phone) throws NoSuchAlgorithmException {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.username = username;
        this.password = getHash(passwd);
        this.telephone = phone;
    }


    protected User(Parcel in) {
        id = in.readLong();
        nom = in.readString();
        prenom = in.readString();
        mail = in.readString();
        username = in.readString();
        password = in.readString();
        telephone = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(mail);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(telephone);
    }

    public String getPassword(){
        return this.password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public boolean is(User user){ return this.id == user.getId(); }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof User))return false;
        User userOther = (User)other;
        return is(userOther);
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte aHash : hash) {
            String hex = Integer.toHexString(0xff & aHash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String getHash(String passwd) throws NoSuchAlgorithmException {
        byte[] hashPasswd;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        hashPasswd = digest.digest(passwd.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(hashPasswd);
    }

}
