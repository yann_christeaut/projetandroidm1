package company.nowhere.yc.projetandroidm1.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.security.NoSuchAlgorithmException;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;
import company.nowhere.yc.projetandroidm1.tools.Validate;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.models.UsersDBHelper;

public class ConnectActivity extends AppCompatActivity {

    private SessionManager session = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        session = new SessionManager(getApplicationContext());
        super.onCreate(savedInstanceState);
        if (session.isLoggedIn()) {
            Intent intent = new Intent(this, TaskOverview.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        else {
            setContentView(R.layout.activity_main);
        }
    }

    public void Connect(View v) throws NoSuchAlgorithmException {
        EditText userField = findViewById(R.id.username);
        String username = userField.getText().toString();
        EditText passwdField = findViewById(R.id.passwd);
        String passwd = User.getHash(passwdField.getText().toString());
        if (Validate.isSafe(username) && Validate.isSafe(passwd)){
            UsersDBHelper userDB = UsersDBHelper.Companion.getInstance(getApplicationContext());
            String uniqId = SessionManager.getUniqueId(getApplicationContext());
            User user = userDB.readUserConnect(username, passwd, uniqId);
            if (user != null) {
                session.createLoginSession(user);
                Intent intent = new Intent(this, TaskOverview.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else{
                showInvalidUser(userField);
                showInvalidUser(passwdField);
            }
        }
        if (!Validate.isSafe(username)){
            showInvalidUser(userField);
        }
        if (!Validate.isSafe(passwd)){
            showInvalidUser(passwdField);
        }
    }

    public void Register(View w){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void showInvalidUser(EditText field){
        field.setError("Le nom d'utilisateur ou mot de passe ne sont pas valide.");
    }
}
