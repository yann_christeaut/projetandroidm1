package company.nowhere.yc.projetandroidm1.controllers;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.media.MediaCas;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.models.Arbre;
import company.nowhere.yc.projetandroidm1.models.ArbreDBHelper;
import company.nowhere.yc.projetandroidm1.models.ArbreViewerDBHelper;
import company.nowhere.yc.projetandroidm1.models.Noeud;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;


public class AdapterUser extends RecyclerView.Adapter<AdapterUser.MyViewHolder> {
    private Arbre a;
    private List<User> mDataset;
    private Context mContext;
    private boolean isShowing;

    private int sizePart;

    AdapterUser(Arbre a,View v, boolean isShowing) {
        this.a = a;
        this.isShowing = isShowing;

        mDataset = new ArrayList<User>() ;

        mDataset.add(a.getProprietaire());
        mDataset.addAll(a.getParticipants());
        mDataset.addAll(a.getViewers());

        sizePart=a.getParticipants().size();
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public AdapterUser.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        // create a new view

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_bubble, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
//        Object task = mDataset.get(position);
        holder.setVal(mDataset.get(position),position,sizePart);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        private User user;
        private int toggleState = 0;
        private SessionManager session = null;
        private int color;
        MyViewHolder(final View itemView) {

            super(itemView);

//            itemView.setOnClickListener(view -> {
//                Intent intent = new Intent(itemView.getContext(), TaskInnerView.class);
//                intent.putExtra("idTask", noeud.getId());
//                intent.putExtra("idTree", noeud.getClosestUpperTree().getId());
//                ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SUB_TASK);
//            });

        }

        void setVal(User user,int position, int sizePart){
            this.user = user;

            String name = String.valueOf(user.getPrenom().charAt(0))+String.valueOf(user.getNom().charAt(0));
            ((TextView)itemView.findViewById(R.id.userName)).setText(name);

            if(position == 0){
                color = R.color.creator;

            }
            else if(position > 0 && position <= sizePart){
                color = R.color.contributor;
            }
            else{
                color = R.color.gest;
            }
            ((TextView)itemView.findViewById(R.id.userName)).setBackgroundTintList(itemView.getResources().getColorStateList(color));

            session = new SessionManager(itemView.getContext().getApplicationContext());
            session.checkLogin();
            User current = session.getUserDetails();

            if(a.getProprietaire().getId() == current.getId() && !isShowing && position!=0) {
                ((Button) itemView.findViewById(R.id.remove_circle)).setVisibility(View.VISIBLE);
            }
            ((Button) itemView.findViewById(R.id.remove_circle)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(v,user);
                }
            });
        }
        public void remove(View v,User u){

            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            builder.setMessage("Êtes-vous sûr de vouloir supprimer "+user.getUsername()+" de cette tache ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Oui", (dialog, which) -> {
                ((Button) itemView.findViewById(R.id.remove_circle)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cancelRemove(v,user);
                    }
                });

                ((TextView)itemView.findViewById(R.id.userName)).setBackgroundTintList(itemView.getResources().getColorStateList(R.color.place_holder));
                ((Button) itemView.findViewById(R.id.remove_circle)).setBackground(itemView.getResources().getDrawable(R.drawable.add_circle));
                ((Button) itemView.findViewById(R.id.remove_circle)).setBackgroundTintList(itemView.getResources().getColorStateList(R.color.btn_add));
                ((TaskInnerView )v.getContext()).addViewerToRemoveToList(u);
            });
            builder.setNegativeButton("Non", (dialog, which) -> dialog.dismiss());
            builder.show();
        }
        public void cancelRemove(View v, User u){
            ((TextView)itemView.findViewById(R.id.userName)).setBackgroundTintList(itemView.getResources().getColorStateList(color));
            ((Button) itemView.findViewById(R.id.remove_circle)).setBackground(itemView.getResources().getDrawable(R.drawable.remove_circle));
            ((Button) itemView.findViewById(R.id.remove_circle)).setBackgroundTintList(itemView.getResources().getColorStateList(R.color.lightRed));
            ((TaskInnerView )v.getContext()).removeViewerToRemoveFromList(u);
            ((Button) itemView.findViewById(R.id.remove_circle)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(v,user);
                }
            });
        }
    }
}