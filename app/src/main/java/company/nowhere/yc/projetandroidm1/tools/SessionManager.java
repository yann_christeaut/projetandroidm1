package company.nowhere.yc.projetandroidm1.tools;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.UUID;

import company.nowhere.yc.projetandroidm1.controllers.ConnectActivity;
import company.nowhere.yc.projetandroidm1.database.DBHelper;
import company.nowhere.yc.projetandroidm1.models.User;

public class SessionManager {
    // Shared Preferences
    final private SharedPreferences pref;
    // Editor for Shared preferences
    final private Editor editor;
    // Context
    final private Context _context;
    // Shared pref mode
    private final int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "TaskManager";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "logged";
    // User ID (make variable public to access from outside)
    private static final String KEY_USERID = "userId";
    // User name address (make variable public to access from outside)
    private static final String KEY_USER = "user";
    // User name address (make variable public to access from outside)
    private static final String KEY_NAME = "name";
    // User surname (make variable public to access from outside)
    private static final String KEY_SURNAME = "surname";
    // Email address (make variable public to access from outside)
    private static final String KEY_EMAIL = "email";
    // User phone address (make variable public to access from outside)
    private static final String KEY_PHONE = "phone";
    // to construct user address (make variable public to access from outside)
    private static final String KEY_PASSWD = "passwd";
    private static final String KEY_DB_VERSION = "db_version";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(User user) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putLong(KEY_USERID, user.getId());
        editor.putString(KEY_USER, user.getUsername());
        editor.putString(KEY_PASSWD, user.getUsername());
        editor.putString(KEY_NAME, user.getNom());
        editor.putString(KEY_SURNAME, user.getPrenom());
        editor.putString(KEY_EMAIL, user.getMail());
        editor.putString(KEY_PHONE, user.getTelephone());
        editor.putInt(KEY_DB_VERSION, DBHelper.DATABASE_VERSION);
        editor.commit();
    }

    /**
     * Check login method wil check user login status If false it will redirect
     * user to login page Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, ConnectActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

    /**
     * Get stored session data
     */
    public User getUserDetails() {
        long userId = pref.getLong(KEY_USERID, -1);
        String name = pref.getString(KEY_NAME, null);
        String prenom = pref.getString(KEY_SURNAME, null);
        String mail = pref.getString(KEY_EMAIL, null);
        String passwd = pref.getString(KEY_PASSWD, null);
        String username = pref.getString(KEY_USER, null);
        String phone = pref.getString(KEY_PHONE, null);
        return new User(userId, name, prenom, mail, username, passwd, phone);
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, ConnectActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getInt(KEY_DB_VERSION, 0) == DBHelper.DATABASE_VERSION && pref.getBoolean(IS_LOGIN, false);
    }

    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public synchronized static String getUniqueId(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }

        return uniqueID;
    }
}