package company.nowhere.yc.projetandroidm1.controllers;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import company.nowhere.yc.projetandroidm1.R;
import company.nowhere.yc.projetandroidm1.models.Arbre;
import company.nowhere.yc.projetandroidm1.models.ArbreDBHelper;
import company.nowhere.yc.projetandroidm1.models.Noeud;
import company.nowhere.yc.projetandroidm1.models.NoeudDBHelper;
import company.nowhere.yc.projetandroidm1.models.User;
import company.nowhere.yc.projetandroidm1.tools.SessionManager;
import company.nowhere.yc.projetandroidm1.tools.Validate;

public class TaskEditItem extends AppCompatActivity {

    private final int DEFAULT_ID = -1;

    private SessionManager session = null;
    private long idNode;
    private long idTree;
    private ArbreDBHelper arbreHelper;
    private NoeudDBHelper noeudHelper;
    private int  y, m, d;
    private DatePickerDialog dialog;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        super.onCreate(savedInstanceState);
        arbreHelper = ArbreDBHelper.Companion.getInstance(getApplicationContext());
        noeudHelper = NoeudDBHelper.Companion.getInstance(getApplicationContext());
        setContentView(R.layout.task_edit_item);

        idNode = getIntent().getLongExtra("idTask", DEFAULT_ID);
        idTree = getIntent().getLongExtra("idTree", DEFAULT_ID);
        if(getIntent().getBooleanExtra("modify",false)){

            Arbre arbre = arbreHelper.readTree(idTree, false);
            Noeud n = noeudHelper.readNoeud(idNode, arbre,true, false);

            ((EditText)findViewById(R.id.editTaskTitle)).setText(n.getTitre());
            ((EditText)findViewById(R.id.editTaskDescrption)).setText(n.getDescription());
            ((EditText)findViewById(R.id.editTaskLocation)).setText( n.getLieu());
            ((SwitchCompat) findViewById(R.id.editTaskEnded)).setChecked(n.getAvancement()>=1);

           ((RadioGroup) findViewById(R.id.editTaskPriority)).check(n.getPriorite());
        }
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy");
        y=Integer.parseInt(df.format(date));
        df = new SimpleDateFormat("MM");
        m=Integer.parseInt(df.format(date));
        df = new SimpleDateFormat("dd");
        d=Integer.parseInt(df.format(date));

        df = new SimpleDateFormat("dd/MM/yyyy");

        ((TextView) findViewById(R.id.editTaskDeadLigne)).setText("Fin le : "+df.format(date));
        dialog = new DatePickerDialog(this,null,  y, m-1, d);
        dialog.setOnDateSetListener((view, year, month, dayOfMonth) ->  {y=year;m=month;d= dayOfMonth;((TextView)findViewById(R.id.editTaskDeadLigne)).setText("Fin le : "+d+"/"+(m+1)+"/"+y);});
    }

    public void Validate(View v){
        boolean insetOk = true;

        String title = ""+((EditText)findViewById(R.id.editTaskTitle)).getText();
        String descr = ""+((EditText)findViewById(R.id.editTaskDescrption)).getText();
        String place = ""+((EditText)findViewById(R.id.editTaskLocation)).getText();
        Date currdate = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

        String deadLDate = "" + d+"/"+(m+1)+"/"+y;
        Date deadline = new Date();
        if(!Validate.isDate(deadLDate)) {
            insetOk = false;
            showInvalidTextField(findViewById(R.id.editTaskDeadLigne),"date limite");
        }
        else{
            try {
                deadline = dateformat.parse(deadLDate);
            } catch (Exception e) {}
        }

        int avancement = Noeud.NOT_FINISHED;
        if(((SwitchCompat) findViewById(R.id.editTaskEnded)).isChecked()){
            avancement = Noeud.FINISHED;
        }

        int idRadioButtonChecked = ((RadioGroup) findViewById(R.id.editTaskPriority)).getCheckedRadioButtonId();
        int priority = Noeud.HIGH_PRIORITY;
        switch (idRadioButtonChecked) {
            case R.id.radioButton1:
                priority = Noeud.LOW_PRIORITY;
                break;
            case R.id.radioButton2:
                priority = Noeud.MEDIUM_PRIORITY;
                break;
            case R.id.radioButton3:
                priority = Noeud.HIGH_PRIORITY;
                break;
        }

        if(title.equals("") || descr.equals("")){
            insetOk = false;
        }

        if(insetOk){
            User user = session.getUserDetails();
            if(idNode != DEFAULT_ID && idTree != DEFAULT_ID){
                Arbre arbre = arbreHelper.readTree(idTree, false);
                Noeud noeudParent = noeudHelper.readNoeud(idNode, arbre,true, true);

                Noeud n = new Noeud(title,descr,place,currdate,deadline,noeudParent.getClosestUpperTree(),
                                    noeudParent,avancement,priority,false, null);
                noeudHelper.insertNoeud(n, arbre);
                noeudParent.setAvancement(noeudParent.getAvancement());
                noeudHelper.updateNoeud(noeudParent);
            }else{
                Noeud n = new Noeud(title,descr,place,currdate,deadline,
                                    null,null,avancement,
                                            priority,false, null);
                Arbre a = new Arbre(n,user, false);
                n.setClosestUpperTree(a);
                arbreHelper.insertTree(a);
            }

            Intent intent = new Intent();
            setResult(AdapterItem.RESULT_CODE_UPDATED, intent);
            super.finish();
        }
        else{
            if(title.equals("")){
                showInvalidTextField(findViewById(R.id.editTaskTitle),"titre");
            }
            if(descr.equals("")){
                showInvalidTextField(findViewById(R.id.editTaskDescrption),"description");
            }
        }

    }

    public void showDate(View v){
        dialog.show();
    }
    private void showInvalidTextField(EditText field,String title){
        field.setError("Le champ " + title + " n'est pas correctement remplis");
    }
}
