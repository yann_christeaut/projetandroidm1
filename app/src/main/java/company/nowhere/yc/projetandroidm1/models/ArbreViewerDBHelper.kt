package company.nowhere.yc.projetandroidm1.models

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import android.provider.BaseColumns
import company.nowhere.yc.projetandroidm1.database.DBContract
import company.nowhere.yc.projetandroidm1.database.DBHelper
import company.nowhere.yc.projetandroidm1.tools.Singleton

/*
    Gere toutes les interactions avec la table ArbreViewer dans la BD
    @param DBHelper : instance de la classe DBHelper
 */
class ArbreViewerDBHelper private constructor(context : Context) {

    private val helper = DBHelper.getInstance(context)
    private val context = context

    /*
        Insere un viewer/participant dans la BD
        @param viewer : User a ajouter dans la BD
        @param mayModify : Boolean true si l'utilisateur est un participant
        @param arbreId : Int id de l'arbre ou ajouter un viewer
        @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun insertViewer(viewer : User, mayModify : Boolean, arbreId : Long) {
        // Gets the data repository in write mode
        val db = helper.writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE, arbreId)
        values.put(DBContract.ArbreViewerEntry.COLUMN_ID_USER, viewer.id)
        values.put(DBContract.ArbreViewerEntry.COLUMN_CAN_MODIFY, mayModify)

        // Insert the new row, returning the primary key value of the new row
        db.insert(DBContract.ArbreViewerEntry.TABLE_NAME, null, values)
        db.close()
    }

    /*
        Insere un une liste de viewers/participants dans la BD
        @param viewers : ArrayList<User> a ajouter dans la BD
        @param mayModify : Boolean true si l'utilisateur est un participant
        @param arbreId : Int id de l'arbre ou ajouter la liste de viewer
        @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun insertViewers(viewers : ArrayList<User>, mayModify : Boolean, arbreId : Long) {
        // Gets the data repository in write mode
        val db = helper.writableDatabase

        for (user : User in viewers){
            // Create a new map of values, where column names are the keys
            val values = ContentValues()
            values.put(DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE, arbreId)
            values.put(DBContract.ArbreViewerEntry.COLUMN_ID_USER, user.id)
            values.put(DBContract.ArbreViewerEntry.COLUMN_CAN_MODIFY, mayModify)

            // Insert the new row, returning the primary key value of the new row
            db.insert(DBContract.ArbreViewerEntry.TABLE_NAME, null, values)
        }
        db.close()
    }

    /*
        Supprime un viewer d'une tache
        @param arbre : Arbre ou supprimer un viewer
        @param user : User a supprimer
        @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun deleteViewer(arbre : Arbre, user: User) {
        // Gets the data repository in write mode
        val db = helper.writableDatabase
        // Define 'where' part of query.
        val whereClause = DBContract.ArbreViewerEntry.COLUMN_ID_USER + " = ? AND " + DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE + " = ?"
        // Specify arguments in placeholder order.
        val whereArgs = arrayOf(user.id.toString(), arbre.id.toString())
        // Issue SQL statement.
        db.delete(DBContract.ArbreViewerEntry.TABLE_NAME, whereClause, whereArgs)
        db.close()
    }

    /*
        Supprime tous les viewers/participants d'une tache
        @param arbre : Arbre ou supprimer les participants
        @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    @Throws(SQLiteConstraintException::class)
    fun deleteAllViewers(arbre: Arbre) {
        // Gets the data repository in write mode
        val db = helper.writableDatabase
        // Define 'where' part of query.
        val whereClause = DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE + " = ?"
        // Specify arguments in placeholder order.
        val whereArgs = arrayOf(arbre.id.toString())
        // Issue SQL statement.
        db.delete(DBContract.ArbreViewerEntry.TABLE_NAME, whereClause, whereArgs)
        db.close()
    }

    /*
        Lis tous les viewers ou participants d'un arbre
        @param arbreId : Int id de l'arbre dans la BD
        @param mayModify : Boolean pour lire tous les viewers ou participant
        @throws SQLiteConstraintException si l'instance de la DBHelper n'a pas les droits en ecriture
     */
    fun readViewers(arbreId: Long, mayModify: Boolean): ArrayList<User?> {
        val mayModifyInt = if (mayModify) 1 else 0
        val viewers = ArrayList<User?>()
        val db = helper.readableDatabase
        val cursor: Cursor?
        try {
            cursor = db.rawQuery("SELECT * " +
                    "FROM " + DBContract.ArbreViewerEntry.TABLE_NAME +
                    " WHERE " + DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE + "='" + arbreId + "'" +
                    " AND " + DBContract.ArbreViewerEntry.COLUMN_CAN_MODIFY + "='" + mayModifyInt + "'" , null)
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES)
            return viewers
        }

        if (cursor!!.moveToFirst()) {
            var userId : Long
            while (!cursor.isAfterLast) {
                userId = cursor.getLong(cursor.getColumnIndex(DBContract.ArbreViewerEntry.COLUMN_ID_USER))
                viewers.add(UsersDBHelper.getInstance(context).readUser(userId))
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()
        return viewers
    }

    companion object : Singleton<ArbreViewerDBHelper, Context>(::ArbreViewerDBHelper) {

        const val SQL_CREATE_ENTRIES =
                "CREATE TABLE ${DBContract.ArbreViewerEntry.TABLE_NAME} (" +
                        "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                        "${DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE} INTEGER NOT NULL," +
                        "${DBContract.ArbreViewerEntry.COLUMN_ID_USER} INTEGER NOT NULL," +
                        "${DBContract.ArbreViewerEntry.COLUMN_CAN_MODIFY} INTEGER NOT NULL," +
                        "FOREIGN KEY(${DBContract.ArbreViewerEntry.COLUMN_ID_ARBRE}) " +
                        "REFERENCES ${DBContract.ArbreEntry.TABLE_NAME}(${BaseColumns._ID}) ON DELETE CASCADE, " +
                        "FOREIGN KEY(${DBContract.ArbreViewerEntry.COLUMN_ID_USER}) " +
                        "REFERENCES ${DBContract.UserEntry.TABLE_NAME}(${BaseColumns._ID}) ON DELETE CASCADE)"

        const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DBContract.ArbreViewerEntry.TABLE_NAME
    }
}